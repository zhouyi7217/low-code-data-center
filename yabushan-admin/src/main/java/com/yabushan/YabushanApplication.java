package com.yabushan;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.activiti.spring.boot.SecurityAutoConfiguration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 启动程序
 *
 * @author yabushan
 */

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class },scanBasePackages = {"org.jeecg.modules.jmreport","com.yabushan"})
@Slf4j
//@MapperScan(value={"org.jeecg.modules.**.mapper*"})
public class YabushanApplication
{
    public static void main(String[] args) throws UnknownHostException {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        ConfigurableApplicationContext application = SpringApplication.run(YabushanApplication.class, args);
        Environment env = application.getEnvironment();
        String applicationName = env.getProperty("server.application.name");
        String path = env.getProperty("server.context-path", "");
        String port = env.getProperty("server.port");
        String host = InetAddress.getLocalHost().getHostAddress();
        String local = String.format("Local: \t\thttp://localhost:%s%s\n\t", port, path);
        String External = String.format("External-ip: \t\thttp://%s:%s%s\n\t", host, port, path);
        String swaggerDoc = String.format("swaggerDoc: \t\thttp://%s:%s%s/swagger-ui.html\n\t", host, port, path);
        String knife4jDoc = String.format("knife4jDoc: \t\thttp://%s:%s%s/doc.html\n\t", host, port, path);

        log.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        local +
                        External +
                        swaggerDoc +
                        knife4jDoc +
                        "----------------------------------------------------------",
                applicationName);

    }
}
