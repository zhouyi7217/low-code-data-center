package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubEducation;
import com.yabushan.system.service.IEmpSubEducationService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工教育子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/education")
public class EmpSubEducationController extends BaseController
{
    @Autowired
    private IEmpSubEducationService empSubEducationService;

    /**
     * 查询员工教育子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:education:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubEducation empSubEducation)
    {
        startPage();
        List<EmpSubEducation> list = empSubEducationService.selectEmpSubEducationList(empSubEducation);
        return getDataTable(list);
    }

    /**
     * 导出员工教育子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:education:export')")
    @Log(title = "员工教育子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubEducation empSubEducation)
    {
        List<EmpSubEducation> list = empSubEducationService.selectEmpSubEducationList(empSubEducation);
        ExcelUtil<EmpSubEducation> util = new ExcelUtil<EmpSubEducation>(EmpSubEducation.class);
        return util.exportExcel(list, "education");
    }

    /**
     * 获取员工教育子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:education:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubEducationService.selectEmpSubEducationById(recId));
    }

    /**
     * 新增员工教育子集
     */
    @PreAuthorize("@ss.hasPermi('system:education:add')")
    @Log(title = "员工教育子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubEducation empSubEducation)
    {
        return toAjax(empSubEducationService.insertEmpSubEducation(empSubEducation));
    }

    /**
     * 修改员工教育子集
     */
    @PreAuthorize("@ss.hasPermi('system:education:edit')")
    @Log(title = "员工教育子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubEducation empSubEducation)
    {
        return toAjax(empSubEducationService.updateEmpSubEducation(empSubEducation));
    }

    /**
     * 删除员工教育子集
     */
    @PreAuthorize("@ss.hasPermi('system:education:remove')")
    @Log(title = "员工教育子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubEducationService.deleteEmpSubEducationByIds(recIds));
    }
}
