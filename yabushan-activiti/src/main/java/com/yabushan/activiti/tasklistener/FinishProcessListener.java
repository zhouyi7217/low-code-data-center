package com.yabushan.activiti.tasklistener;

import org.activiti.engine.delegate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 请假流程
 * @author yabushan
 *
 */
@Service
public class FinishProcessListener implements TaskListener, JavaDelegate {

	@Override
	public void notify(DelegateTask execution)  {


	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		//流程完成时，修改业务数据表状态
		String BID = (String) execution.getVariable("BID");
		Long bid=Long.valueOf(BID);

	}
}
