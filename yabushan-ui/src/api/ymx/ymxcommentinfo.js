import request from '@/utils/request'

// 查询评论列表
export function listYmxcommentinfo(query) {
  return request({
    url: '/ymx/ymxcommentinfo/list',
    method: 'get',
    params: query
  })
}

// 查询评论详细
export function getYmxcommentinfo(commentId) {
  return request({
    url: '/ymx/ymxcommentinfo/' + commentId,
    method: 'get'
  })
}

// 新增评论
export function addYmxcommentinfo(data) {
  return request({
    url: '/ymx/ymxcommentinfo',
    method: 'post',
    data: data
  })
}

// 修改评论
export function updateYmxcommentinfo(data) {
  return request({
    url: '/ymx/ymxcommentinfo',
    method: 'put',
    data: data
  })
}

// 删除评论
export function delYmxcommentinfo(commentId) {
  return request({
    url: '/ymx/ymxcommentinfo/' + commentId,
    method: 'delete'
  })
}

// 导出评论
export function exportYmxcommentinfo(query) {
  return request({
    url: '/ymx/ymxcommentinfo/export',
    method: 'get',
    params: query
  })
}